#version 330 core
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VS_OUT {
    vec3 normal;
    vec2 intUV;
} gs_in[];

out vec3 vNormal;
out vec2 interpUV;

void main() {
  for(int i = 0; i < 3; i++) { 
    gl_Position = gl_in[i].gl_Position;
    vNormal = gs_in[i].normal;
    interpUV = gs_in[i].intUV;
    EmitVertex();
  }
  EndPrimitive();
}