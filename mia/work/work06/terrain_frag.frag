/*
24_cooktorrance_tex_dirlight.frag: fragment shader for Cook-Torrance, with texturing.
It consider a single directional light.

author: Davide Gadia

Real-time Graphics Programming - a.a. 2017/2018
Master degree in Computer Science
Universita' degli Studi di Milano

*/

#version 330 core

// output shader variable
out vec4 colorFrag;

// light incidence direction (calculated in vertex shader, interpolated by rasterization)
in vec3 lightDir;
// the transformed normal has been calculated per-vertex in the vertex shader
in vec3 vNormal;
in vec3 tangent;
in vec3 bitangent;
in mat3 TBN;
uniform mat3 normalMatrix;
// vector from fragment to camera (in view coordinate)
in vec3 vViewPosition;
flat in int apply_norm;
// vector from fragment to camera (in view coordinate)
in vec2 interp_UV;

in vec4 posLightSpace;
in float fog_factor;
in float height;
// texture repetitions
uniform float repeat;


// texture sampler
uniform sampler2D tex;
uniform sampler2D shadowMap;
uniform sampler2D map_tex;
uniform sampler2D alt_tex;
uniform sampler2D previous_alt_map;
uniform sampler2D bump_tex;
uniform int vps;  //vertex per side
uniform float dbg_val;
uniform float dbg_val2;
uniform int normap_type;
uniform int n_tiles;


uniform float frequency;
uniform float power;

// we must copy and paste the code inside our shaders
// it is not possible to include or to link an external file
////////////////////////////////////////////////////////////////////
// Description : Array and textureless GLSL 2D/3D/4D simplex
//               noise functions.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : ijm
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//
float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x) {
     return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

float snoise(vec3 v)
  {
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

// First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

// Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //   x0 = x0 - 0.0 + 0.0 * C.xxx;
  //   x1 = x0 - i1  + 1.0 * C.xxx;
  //   x2 = x0 - i2  + 2.0 * C.xxx;
  //   x3 = x0 - 1.0 + 3.0 * C.xxx;
  vec3 x1 = x0 - i1 + C.xxx;
  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
  vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y

// Permutations
  i = mod289(i);
  vec4 p = permute( permute( permute(
             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
           + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))
           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

// Gradients: 7x7 points over a square, mapped onto an octahedron.
// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
  float n_ = 0.142857142857; // 1.0/7.0
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;
  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;
  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

//Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

// Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1),
                                dot(p2,x2), dot(p3,x3) ) );
  }

/*
  float r = power*snoise(vec3(interp_UV*frequency, 0.4));
  float g = power*snoise(vec3(interp_UV*frequency, -0.7));
  float b = power*snoise(vec3(interp_UV*frequency, 0.8));

  vec4 color = vec4(r,g,b,1.0);
*/


uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}


float random( float f ) {
    const uint mantissaMask = 0x007FFFFFu;
    const uint one          = 0x3F800000u;
    
    uint h = hash( floatBitsToUint( f ) );
    h &= mantissaMask;
    h |= one;
    
    float  r2 = uintBitsToFloat( h );

    return r2;
}

float stupid(vec2 f){

    float x = rand(vec2(int(f.x*(250)),int(f.y*(250))));
    if(x<0)
        x=-1*x;
    return x;
}

float scale(float f, float max){
    // 0<=f,max<=1      1::max = f::x
    float toret = f*max;
    
   /* applicato al valore del perlin noise,flippare i valori negativi fa tipo effetto piasterllato 
    if(toret<0)
        toret=toret*-1;
    */

    return toret;
}

float stupidt(float f){
    float r = random(f);
    if(r<0)
        r=-1*r;
    r=mod(r,n_tiles)+dbg_val;
    return r;
}

void main()
{
    // we repeat the UVs and we sample the texture
    vec2 repeated_Uv = mod(interp_UV*repeat, 1.0);
    vec4 surfaceColor;
    float grad = texture(map_tex,interp_UV).r;

//  vec4(61.0f/255.0f, 124.0f/255.0f, 56.0f/255.0f,0)
//vec4(109.0f/255.0f, 56.0f/255.0f, 127.0f/255.0f,0)
//  
//  
    surfaceColor = texture(alt_tex, repeated_Uv)*grad+texture(tex, repeated_Uv)*(1.0f-grad);

//    vec4 fog_color = vec4(0.0f,0.0f,0.0f,1.0f);
//    surfaceColor = (1.0f-fog_factor)*fog_color + fog_factor*surfaceColor;

    //soluzione 1 sporca: al bordo metto il colore di base
/*    if(interp_UV.y>0.9f){
//      dist : 0.1 = x : 1
        float grad = 1.0f-(1.0f-interp_UV.y)/0.1f;
        surfaceColor = texture(alt_tex, repeated_Uv)*grad+surfaceColor*(1.0f-grad);
    }
*/

    if(interp_UV.y>0.95f){
        float dist = 1.0f-interp_UV.y;
        float prev_grad = texture(previous_alt_map,vec2(interp_UV.x,dist)).r;
        vec4 old_surface =  texture(alt_tex, repeated_Uv)*prev_grad+texture(tex, repeated_Uv)*(1.0f-prev_grad);
        float prev_influence = (interp_UV.y-0.95f)/0.05f;
        surfaceColor = surfaceColor*(1.0f-prev_influence)+(old_surface*prev_influence);

    }

    vec3 N;
    if(normap_type==1 && apply_norm==1){

        /*
        float x =1.0f*snoise(vec3(repeated_Uv*2, interp_UV.x*2-1));
        float y = 1.0f*snoise(vec3(repeated_Uv*2,interp_UV.y*2-1));
        float z = 1.0f*snoise(vec3(repeated_Uv*2, (interp_UV.x+interp_UV.y)/2.0f)*2-1);


         (mod(random(square_num_x),10)/5.0f) 
        */
        float square_num_x = vps*interp_UV.x;
        float square_num_y = vps*interp_UV.y;

/*
        float x = (mod(random(square_num_x),10)/5.0f) *snoise(vec3(repeated_Uv*2,1));
        float y = (mod(random(square_num_x),10)/5.0f) *snoise(vec3(repeated_Uv*2,-0.7));
        float z = (mod(random(square_num_x),10)/5.0f) *snoise(vec3(repeated_Uv*2, 1));
*/

        /* S1
        float x = dbg_val+0.3+1.0f*snoise(vec3(repeated_Uv,stupid(vec2(interp_UV.y,interp_UV.y) )));
        float y = dbg_val+0.3+1.0f*snoise(vec3(repeated_Uv,stupid(vec2(interp_UV.x,interp_UV.x))));
        float z = 0.8+snoise(vec3(repeated_Uv, stupid(interp_UV.xy)));
        */
        float x = scale( snoise(vec3(interp_UV*1000*(3.5f),123)),0.4f );
        float y = scale( snoise(vec3(interp_UV*1000*(3.5f),321)),0.4f );
        float z = 0.88f+scale( snoise(vec3(interp_UV*1000*(3.5f), 213)),0.12f );

        vec3 bumped = vec3(x,y,z);
        //N = TBN*normalize(texture(bump_tex,repeated_Uv).rgb*2.0f-1.0f);
        N = TBN*(normalize( (bumped) ));

    }
    else if(normap_type==3){
        N = TBN*normalize((texture(bump_tex,repeated_Uv).rgb*2.0f-1.0f));
    }
    else{
        N = vNormal;
    }
    vec3 L = normalize(lightDir.xyz);
    float lambertian = max(dot(L,N), 0.0);

    if(normap_type==2){
        /* "soluzione" 1
        float x = dbg_val+0.5f+scale( snoise(vec3(repeated_Uv,stupid(vec2(interp_UV.y,interp_UV.y) ))),0.5f );
        float y = dbg_val+0.5f+scale( snoise(vec3(repeated_Uv,stupid(vec2(interp_UV.x,interp_UV.x)))),0.5f );
        float z = 0.8f+scale( snoise(vec3(repeated_Uv, stupid(interp_UV.xy))),0.2f );
        surfaceColor = normalize(vec4(x,y,z,1));

        */
 
        float x = scale( snoise(vec3(interp_UV*1000*(3.5f),123)),0.4f );
        float y = scale( snoise(vec3(interp_UV*1000*(3.5f),321)),0.4f );
        float z = 0.88f+scale( snoise(vec3(interp_UV*1000*(3.5f), 213)),0.12f );
        surfaceColor = normalize(vec4(x,y,z,1));
    }
    colorFrag = surfaceColor * lambertian;
}
