/*
work05c

author: Davide Gadia

Real-time Graphics Programming - a.a. 2017/2018
Master degree in Computer Science
Universita' degli Studi di Milano
*/

/*
OpenGL coordinate system (right-handed)
positive X axis points right
positive Y axis points up
positive Z axis points "outside" the screen


                              Y
                              |
                              |
                              |________X
                             /
                            /
                           /
                          Z
*/

#ifdef _WIN32
    #define __USE_MINGW_ANSI_STDIO 0
#endif
// Std. Includes
#include <string>

// Loader for OpenGL extensions
// http://glad.dav1d.de/
// THIS IS OPTIONAL AND NOT REQUIRED, ONLY USE THIS IF YOU DON'T WANT GLAD TO INCLUDE windows.h
// GLAD will include windows.h for APIENTRY if it was not previously defined.
// Make sure you have the correct definition for APIENTRY for platforms which define _WIN32 but don't use __stdcall
#ifdef _WIN32
    #define APIENTRY __stdcall
#endif

#include <glad/glad.h>

// GLFW library to create window and to manage I/O
#include <glfw/glfw3.h>

// another check related to OpenGL loader
// confirm that GLAD didn't include windows.h
#ifdef _WINDOWS_
    #error windows.h was included!
#endif

// classes developed during lab lectures to manage shaders, to load models, and for FPS camera
// in this example, the Model and Mesh classes support texturing
#include <utils/shader_v1.h>
#include <utils/model_v2.h>
#include <utils/camera.h>
#include <utils/TerrainTile.h>
#include <utils/TileGen.h>
#include <stdlib.h>
#include <time.h>

#include <utils/mingw.thread.h>
#include <glm/gtx/string_cast.hpp>
// we load the GLM classes used in the application
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
// we include the library for images loading
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>

int k=0;
// dimensions of application's window
GLuint screenWidth = 1920, screenHeight = 1080;

// callback functions for keyboard and mouse events
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
// if one of the WASD keys is pressed, we call the corresponding method of the Camera class
void apply_camera_movements(TerrainTile *a, TerrainTile *b);
// we put the code for the models rendering in a separate function, because we will apply 2 rendering steps
void RenderObjects(Shader &shader, Model &sphereModel);
void renderLoading(Shader shader, Model &model,glm::mat4 projectionMatrix);
void colorLoading(float f);
void genTerrain( GLint *target, int command=0);
void initShadow();
void boh_func(int *a);
// load image from disk and create an OpenGL texture
GLint LoadTexture(const char* path,GLfloat min_mag_param);
GLint LoadTextureCube(string path);
GLint generateTexture(vector<GLfloat> *target = nullptr);
float pd_val=10.0f;
float sun_off=0.005f;
// we initialize an array of booleans for each keybord key
bool keys[1024];

// we set the initial position of mouse cursor in the application window
GLfloat lastX = 400, lastY = 300;
// when rendering the first frame, we do not have a "previous state" for the mouse, so we need to manage this situation
bool firstMouse = true;
bool interp_tiles = true;
int normap_type=0;
const int N_TILES=1000;
float TILE_SIZE=5.0;
float debug_float=0.0f;
double last_time;
int frames = 0;
float dbg_val;
float dbg_val2=1;
int position_counter=0;
bool tileB_head=true;
bool light_follows_camera=true;
// parameters for time calculation (for animations)
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

// rotation angle on Y axis
GLfloat orientationY = 0.0f;
// rotation speed on Y axis
GLfloat spin_speed = 30.0f;
// boolean to start/stop animated rotation on Y angle
GLboolean spinning = GL_TRUE;

// boolean to activate/deactivate wireframe rendering
GLboolean wireframe = GL_FALSE;

glm::mat4 view;

// we create a camera. We pass the initial position as a paramenter to the constructor. The last boolean tells that we want a camera "anchored" to the ground
Camera camera(glm::vec3((N_TILES*TILE_SIZE)/2, -10.0f, 0), GL_FALSE);
GLFWwindow* window;
glm::vec3 lightPos0 = glm::vec3((N_TILES*TILE_SIZE)/2, 200.0f, 0);

GLfloat Kd = 0.8f;
GLfloat m = 0.3;
GLfloat F0 = 3.0;
GLfloat *heights;
// vector for the textures IDs
vector<GLint> textureID;
GLuint textureCube;


// UV repetitions
GLfloat repeat = 1.0;

int TileGen::n_tiles=N_TILES;
float TileGen::tile_size=TILE_SIZE;
int TileGen::debug=0;

//multithread texture generation:
bool tex_ready;
vector<GLuint> buffer_textureID;
vector<GLfloat> *generated_height = new vector<GLfloat>();
vector<GLfloat> *generated_texmap = new vector<GLfloat>();
vector<GLfloat> *head_heightmap = new vector<GLfloat>();
vector<GLfloat> *tail_heightmap = new vector<GLfloat>();

void th_genAllTexture(int random_seed,vector<GLfloat> *heightmap, vector<GLfloat> *texmap, bool *work_done_flag);	//funzione da lanciare nel thread
void genTexToOpenGL();	// quando finisce th_gen_texture i risultati sono passati a opengl nel mainloop ( nel thread che per opengl è "current")
void genSingleTexToOpengl( int texture_index,GLfloat *img);

/////////////////// MAIN function ///////////////////////
int main()
{

	
	
	
  // Initialization of OpenGL context using GLFW 
  glfwInit();
  // We set OpenGL specifications required for this application
  // In this case: 3.3 Core
  // It is possible to raise the values, in order to use functionalities of OpenGL 4.x
  // If not supported by your graphics HW, the context will not be created and the application will close
  // N.B.) creating GLAD code to load extensions, try to take into account the specifications and any extensions you want to use,
  // in relation also to the values indicated in these GLFW commands
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  // we set if the window is resizable
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  // we create the application's window
    window = glfwCreateWindow(screenWidth, screenHeight, "work05c", nullptr /*glfwGetPrimaryMonitor()*/, nullptr);
    if (!window)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // we put in relation the window and the callbacks
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);

    // we disable the mouse cursor
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // GLAD tries to load the context set by GLFW
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        return -1;
    }


    // we define the viewport dimensions
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

    // we enable Z test
    glEnable(GL_DEPTH_TEST);

    //the "clear" color for the frame buffer
    glClearColor(0.26f, 0.46f, 0.98f, 1.0f);
	// "24_cooktorrance_tex_dirlight.frag
	cout<<"Loading shaders"<<endl;
    Shader object_shader("00_basic.vert","01_fullcolor.frag ");
	cout<<"obj shaders done"<<endl;
	Shader terrain_shader("prova.vert" ,"terrain_frag.frag");
	cout<<"terr shaders done"<<endl;
	Shader loading_shader("loading.vert","loading.frag");
	
	
	Shader sky_shader("sky.vert","sky.frag");
    Shader shadow_shader("23_shadowmap.vert", "23_shadowmap.frag");
	
	
  /////////////////// CREATION OF BUFFER FOR THE  DEPTH MAP /////////////////////////////////////////
    // buffer dimension: too large -> performance may slow down if we have many lights; too small -> strong aliasing
    const GLuint SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
    GLuint depthMapFBO;
    // we create a Frame Buffer Object: the first rendering step will render to this buffer, and not to the real frame buffer
    glGenFramebuffers(1, &depthMapFBO);
    // we create a texture for the depth map
    GLuint depthMap;
    glGenTextures(1, &depthMap);
    glBindTexture(GL_TEXTURE_2D, depthMap);
    // in the texture, we will save only the depth data of the fragments. Thus, we specify that we need to render only depth in the first rendering step
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // imposto di "clampare" per coordinate fuori dallo spazio [0,1]
    // we set to clamp the uv coordinates outside [0,1] to the color of the border
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    // outside the area covered by the light frustum, everything is rendered in shadow (because we set GL_CLAMP_TO_BORDER)
    // thus, we set the texture border to white, so to render correctly everything not involved by the shadow map
    GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    // we bind the depth map FBO
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
    // we set that we are not calculating nor saving color data
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    ///////////////////////////////////////////////////////////////////	
	cout<<"TILE CREATION      -------"<<endl;
	TerrainTile *terrain = new TerrainTile(TILE_SIZE,N_TILES); 
	colorLoading(0.8f);
	cout<<endl;
	TerrainTile *tileB = new TerrainTile(TILE_SIZE,N_TILES); 
	colorLoading(0.7f);
	
	
	// Projection matrix: FOV angle, aspect ratio, near and far planes
    glm::mat4 projection = glm::perspective(45.0f, (float)screenWidth/(float)screenHeight, 0.1f, 200000.0f);

	Model planeModel("../../../models/plane.obj");
	colorLoading(0.0f);

	srand (time(NULL));

	cout<<"TEXTURE GENERATION -------"<<endl;
	vector<GLfloat> *temp_height = new vector<GLfloat>();
	textureID.push_back(generateTexture(tail_heightmap));	//tile A heightmap 								0
	colorLoading(0.1f);
	textureID.push_back(generateTexture(head_heightmap));	//tile B heightmap 								1
	colorLoading(0.2f);
	textureID.push_back(generateTexture()); //tile NEW heightmap 							2
	colorLoading(0.3f);
	textureID.push_back(generateTexture());	//tile A textureMap  							3
	colorLoading(0.4f);
	textureID.push_back(generateTexture());	//tile B textureMap								4
	colorLoading(0.5f);
	textureID.push_back(generateTexture());	//tile NEW textureMap							5
	colorLoading(0.6f);
	cout<<"TEXTURE LOADING    -------"<<endl;
	textureID.push_back(LoadTexture("../../../textures/dirt.jpg",GL_NEAREST_MIPMAP_NEAREST));	//						7
	textureID.push_back(LoadTexture("../../../textures/rock.png",GL_NEAREST_MIPMAP_NEAREST));	//toPaint textures      6
	textureID.push_back(LoadTexture("../../../textures/bump3.jpg",GL_NEAREST));	//						8
    textureCube = LoadTextureCube("../../../textures/ame_nebula/ame_nebula/");
	cout<<"MODELS LOADING     -------"<<endl;
    Model cubeModel("../../../models/cube.obj");
    Model sphereModel("../../../models/sphere.obj");
    Model bunnyModel("../../../models/bunny_lp.obj");
	colorLoading(0.9f);

	//@ADD
	cout<<"SET TILE           -------"<<endl;
	terrain->setTexIndex(6,7,3,4);
	terrain->setHeightmapIndex(0,1);
	terrain->translate(glm::vec3(0,1,10));
	colorLoading(0.6f);
	tileB->setTexIndex(6,7,4,3);
	tileB->setHeightmapIndex(1,0);
	tileB->translate(glm::vec3(0,1.0f,10+-1*tileB->getSize()*tileB->getQuadNum()));
	colorLoading(0.6f);
    // Rendering loop: this code is executed at each frame
	last_time = glfwGetTime();
    while(!glfwWindowShouldClose(window))		//mainloop
    {
		//fps counter
		double current_time = glfwGetTime();
		frames++;
		if(current_time - last_time>1.0){
			cout<<frames<<" FPS"<<endl;
			last_time = current_time;
			frames=0;
		
		}
		
        // we determine the time passed from the beginning
        // and we calculate time difference between current frame rendering and the previous one
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // Check is an I/O event is happening
        glfwPollEvents();
        // we apply FPS camera movements
        apply_camera_movements(terrain,tileB);

		if(light_follows_camera){
			lightPos0 = camera.Position;
			lightPos0.y+=250;
		}
		/////////////////////////// MOVE/GEN -> Manage Terrain ///////////////////////
		if(tex_ready){	//texture generation in thread is over
			cout<<"generazione texture::main thread "<<endl;
			genTexToOpenGL();
			tex_ready=false;
			buffer_textureID.clear();
			cout<<"generazione texture::main thread -> fine"<<endl;
		}
		
		if(pd_val!=0.0f){
			tileB->translate(glm::vec3(0,0,-pd_val));
			terrain->translate(glm::vec3(0,0,-pd_val));
			TerrainTile *head;
			TerrainTile *tail;
			if(tileB_head){
				head=tileB;
				tail=terrain;
			}
			else{
				head=terrain;
				tail=tileB;
			}
			glm::vec3 pos,pos_t;
			head->getPosition(&pos);
			tail->getPosition(&pos_t);
			//cout<<"tileB head"<<tileB_head<<" POS Z"<<pos.z<<" "<<pos.x<<" "<<pos.y<<" "<<head->getQuadNum()<<endl;
			if(pos.z<=-1*TILE_SIZE*N_TILES){
				float offset =abs(pos.z)-TILE_SIZE*N_TILES;
				//head->translate(glm::vec3(0,0,offset+2.0*(TILE_SIZE)*(N_TILES)));
				head->translate(pos_t-pos+glm::vec3(0,0,(TILE_SIZE*N_TILES)));
				textureID[head->heightmap_index] = textureID[2];
				textureID[head->alternative_texture_map] = textureID[5];

				int seed=rand();
				std::thread t1(th_genAllTexture,seed,generated_texmap,generated_height ,&tex_ready);
				t1.detach();
				//th_genAllTexture(generated_texmap,generated_height ,&tex_ready);
				cout<<k<<endl;
				tail->prev_heightmap_index=head->heightmap_index;
				tail->prev_alt_map = head->alternative_texture_map;
				
				
				tileB_head=!tileB_head;
				//head_heightmap = vector<GLfloat>(tail_heightmap);
				
				
			}
		}
		
		///////////////////////////////////////////////////////////////////////////////////////////////////
		
        view = camera.GetViewMatrix();

        // we "clear" the frame and z buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // we set the rendering mode
        if (wireframe)
            // Draw in wireframe
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        // if animated rotation is activated, than we increment the rotation angle using delta time and the rotation speed parameter
        if (spinning)
            orientationY+=(deltaTime*spin_speed);

        /////////////////// PLANE ////////////////////////////////////////////////
        // We render a plane under the objects. We apply the fullcolor shader to the plane, and we do not apply the rotation applied to the other objects.
        terrain_shader.Use();

        glUniformMatrix4fv(glGetUniformLocation(terrain_shader.Program, "projectionMatrix"), 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(terrain_shader.Program, "viewMatrix"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix3fv(glGetUniformLocation(terrain_shader.Program, "cameraPos"), 1, GL_FALSE, glm::value_ptr(camera.Position));

		
		glUniform3fv(glGetUniformLocation(terrain_shader.Program,"lightPos"),1,glm::value_ptr(lightPos0));

		
		glUniform1f(glGetUniformLocation(terrain_shader.Program,"roughness"),15.0f);

   
		glUniform1f(glGetUniformLocation(terrain_shader.Program, "pd"),pd_val);
		glUniform1f(glGetUniformLocation(terrain_shader.Program,"dbg_val"),dbg_val);
		glUniform1i(glGetUniformLocation(terrain_shader.Program, "n_tiles"),N_TILES);
		glUniform1f(glGetUniformLocation(terrain_shader.Program,"dbg_val2"),dbg_val2);

		terrain->uniformTextures(terrain_shader,textureID);
		terrain->prepareDefaultUniforms(terrain_shader,view);
		tileB->merge_tiles=interp_tiles;
		terrain->merge_tiles=interp_tiles;
		glUniform1i(glGetUniformLocation(terrain_shader.Program,"normap_type"), normap_type );	


		glUniform1f(glGetUniformLocation(terrain_shader.Program,"tile_size"),TILE_SIZE);
		terrain->Draw(terrain_shader);
		
		
		tileB->uniformTextures(terrain_shader,textureID);
		tileB->prepareDefaultUniforms(terrain_shader,view);

			

		tileB->Draw(terrain_shader);
		
		object_shader.Use();

        glUniformMatrix4fv(glGetUniformLocation(object_shader.Program, "projectionMatrix"), 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(object_shader.Program, "viewMatrix"), 1, GL_FALSE, glm::value_ptr(view));


		RenderObjects(object_shader, sphereModel);

        glDepthFunc(GL_LEQUAL);
        sky_shader.Use();
        // we activate the cube map
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, textureCube);
         // we pass projection and view matrices to the Shader Program of the skybox
        glUniformMatrix4fv(glGetUniformLocation(sky_shader.Program, "projectionMatrix"), 1, GL_FALSE, glm::value_ptr(projection));
        // to have the background fixed during camera movements, we have to remove the translations from the view matrix
        // thus, we consider only the top-left submatrix, and we create a new 4x4 matrix
        view = glm::mat4(glm::mat3(camera.GetViewMatrix()));    // Remove any translation component of the view matrix
        glUniformMatrix4fv(glGetUniformLocation(sky_shader.Program, "viewMatrix"), 1, GL_FALSE, glm::value_ptr(view));

		GLint textureLocation;
        // we determine the position in the Shader Program of the uniform variables
        textureLocation = glGetUniformLocation(sky_shader.Program, "tCube");
        // we assign the value to the uniform variable
        glUniform1i(textureLocation, 0);

        // we render the cube with the environment map
        cubeModel.Draw(sky_shader);


        glfwSwapBuffers(window);
    }
    terrain_shader.Delete();
	object_shader.Delete();
    glfwTerminate();
    return 0;
}


//////////////////////////////////////////
void RenderObjects(Shader &shader,  Model &sphereModel)
{
	shader.Use();
	
	GLint textureLocation = glGetUniformLocation(shader.Program, "tex");
    GLint repeatLocation = glGetUniformLocation(shader.Program, "repeat");

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID[0]);
    glUniform1i(textureLocation, 0);
    glUniform1f(repeatLocation, repeat);

    glm::mat4 sphereModelMatrix;
    glm::mat3 sphereNormalMatrix;
	GLint planeColorLocation = glGetUniformLocation(shader.Program, "colorIn");
        // we assign the value to the uniform variables
	GLfloat planeColor[] = {0.7,0.8,0.8};
	glUniform3fv(planeColorLocation, 1, planeColor);
    sphereModelMatrix = glm::translate(sphereModelMatrix, lightPos0);
    sphereModelMatrix = glm::rotate(sphereModelMatrix, glm::radians(orientationY), glm::vec3(0.0f, 1.0f, 0.0f));
    sphereModelMatrix = glm::scale(sphereModelMatrix, glm::vec3(60.0f, 60.0f, 60.0f));
    // se casto a mat3 una mat4, in automatico estraggo la sottomatrice 3x3 superiore sinistra
    sphereNormalMatrix = glm::inverseTranspose(glm::mat3(view*sphereModelMatrix));
    glUniformMatrix4fv(glGetUniformLocation(shader.Program, "modelMatrix"), 1, GL_FALSE, glm::value_ptr(sphereModelMatrix));
    glUniformMatrix3fv(glGetUniformLocation(shader.Program, "normalMatrix"), 1, GL_FALSE, glm::value_ptr(sphereNormalMatrix));

    // renderizza il modello
    sphereModel.Draw(shader);	


}

void renderLoading(Shader shader, Model &model,glm::mat4 projectionMatrix){
	glClearColor(0.46f, 0.46f, 0.46f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	shader.Use();
	view = camera.GetViewMatrix();
	glm::mat4 modelMatrix = glm::translate(modelMatrix,glm::vec3((N_TILES*TILE_SIZE)/2, -10.0f, -3));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "viewMatrix"), 1, GL_FALSE, glm::value_ptr(view));
    glm::mat3 normalMatrix= glm::inverseTranspose(glm::mat3(view*modelMatrix));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "modelMatrix"), 1, GL_FALSE, glm::value_ptr(modelMatrix));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "projectionMatrix"), 1, GL_FALSE, glm::value_ptr(projectionMatrix));
	model.Draw(shader);

}

void colorLoading(float f){
	glClearColor(f,f,f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glfwSwapBuffers(window);
}
//////////////////////////////////////////
// we load the image from disk and we create an OpenGL texture

GLint LoadTexture(const char* path, GLfloat min_mag_param)
{
    GLuint textureImage;
    int w, h, channels;
    unsigned char* image;
    image = stbi_load(path, &w, &h, &channels, STBI_rgb);

    if (image == nullptr)
        std::cout << "Failed to load texture " <<path<< std::endl;

    glGenTextures(1, &textureImage);
    glBindTexture(GL_TEXTURE_2D, textureImage);
    // 3 channels = RGB ; 4 channel = RGBA
    if (channels==3)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    else if (channels==4)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		
	cout<<"Loaded Texture at"<<path<<" dim:"<<w<<" "<<h<<endl;
    glGenerateMipmap(GL_TEXTURE_2D);
    // we set how to consider UVs outside [0,1] range
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    // we set the filtering for minification and magnification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_mag_param);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, min_mag_param);

    // we free the memory once we have created an OpenGL texture
    stbi_image_free(image);

	cout<<"INT ret "<<textureImage<<endl;
    return textureImage;

}

GLint generateTexture(vector<GLfloat> *target){

	cout<<"Generating texture..."<<endl;
	
	GLuint texture;
	int tex_res = N_TILES;
	vector<GLfloat> *img = new vector<GLfloat>();
	TileGen::getTerrain(rand(),img);
	GLfloat * boh = new GLfloat[tex_res*tex_res];
	int pos=0;
	for(int i=0; i<tex_res*tex_res; i++){
		float num = (img->at(i));
		boh[i] = num;
		if(target!=nullptr){
			target->push_back(num);
		}
	}	
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, tex_res, tex_res, 0, GL_RED, GL_FLOAT, boh);
    glGenerateMipmap(GL_TEXTURE_2D);
    // we set how to consider UVs outside [0,1] range
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    // we set the filtering for minification and magnification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);

    // we free the memory once we have created an OpenGL texture
    stbi_image_free(boh);
	delete img;
    return texture;
}
//////////////////////////////////////////
// If one of the WASD keys is pressed, the camera is moved accordingly (the code is in utils/camera.h)
void apply_camera_movements(TerrainTile *a, TerrainTile *b)
{
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
	/*
	glm::vec3 tilePosition;
	if(tileB_head)
		b->getPosition(&tilePosition);
	else
		a->getPosition(&tilePosition);
	glm::vec3 relative_pos = camera.Position-tilePosition;
	
	int index = ((int) relative_pos.x/(TILE_SIZE) )*2*N_TILES + ((int)relative_pos.z/(TILE_SIZE));
	float fi = 1.0f;
	if(index>=0 && index<head_heightmap.size())
		fi=(((head_heightmap.at(index)+1.0f)/2.0f)-0.5f)*100.0f-1.0f;
	debug_float = fi;
	cout<< head_heightmap.at(index) <<" "<<(int) relative_pos.z/(TILE_SIZE)<<" "<<(int)relative_pos.x/(TILE_SIZE)<<" "<<fi<<endl;

	cout<<((int) relative_pos.z *2*N_TILES)<<" "<<(int)relative_pos.x *2*N_TILES <<endl;
	cout<<"RELATIVE CAMERA POSITION"<<relative_pos.x/(TILE_SIZE*N_TILES)<<" "<<relative_pos.z/(TILE_SIZE*N_TILES)<<endl;
	cout<<"POS"<<((int) relative_pos.z *2*N_TILES)+((int)relative_pos.x )<<endl<<"MAX"<<head_heightmap.size()<<endl<<" H:"<<h<<" "<<fi<<endl;
*/
}


//////////////////////////////////////////
// callback for keyboard events
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    // if ESC is pressed, we close the application
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    // if P is pressed, we start/stop the animated rotation of models
    if(key == GLFW_KEY_P && action == GLFW_PRESS)
        spinning=!spinning;

	float light_spd = 100.0f;
    if(key == GLFW_KEY_I && action == GLFW_PRESS){lightPos0.y+=light_spd; light_follows_camera=false;}
	if(key == GLFW_KEY_K && action == GLFW_PRESS){lightPos0.y-=light_spd;light_follows_camera=false;}

	if(key == GLFW_KEY_L && action == GLFW_PRESS){lightPos0.x+=light_spd;light_follows_camera=false;}
	if(key == GLFW_KEY_J && action == GLFW_PRESS){lightPos0.x-=light_spd;light_follows_camera=false;}
	if(key == GLFW_KEY_O && action == GLFW_PRESS){lightPos0.z+=light_spd;light_follows_camera=false;}
	if(key == GLFW_KEY_U && action == GLFW_PRESS){lightPos0.z-=light_spd;light_follows_camera=false;}
	if(key == GLFW_KEY_H && action == GLFW_PRESS){
		light_follows_camera=!light_follows_camera;		
	}
    // if L is pressed, we activate/deactivate wireframe rendering of models
    if(key == GLFW_KEY_1 && action == GLFW_PRESS)
        wireframe=!wireframe;
	if(key == GLFW_KEY_2 && action == GLFW_PRESS)
		interp_tiles = !interp_tiles;
	if(key == GLFW_KEY_3 && action == GLFW_PRESS){						// (0 no normal) (1 rand normal map) (2 rand normal to color) (3 normal from file)	 
		normap_type=0;
		cout<<"Normal map type = "<<normap_type<<endl;
	}
	if(key == GLFW_KEY_4 && action == GLFW_PRESS){
		if(normap_type!=1)
			normap_type=1;
		else
			normap_type=0;
		
		
		cout<<"Normal map type = "<<normap_type<<endl;
	}
	if(key == GLFW_KEY_5 && action == GLFW_PRESS){
		if(normap_type!=2)
			normap_type=2;
		else
			normap_type=0;
		cout<<"Normal map type = "<<normap_type<<endl;
	}	
	if(key == GLFW_KEY_6 && action == GLFW_PRESS){
		if(normap_type!=3)
			normap_type=3;
		else
			normap_type=0;
		cout<<"Normal map type = "<<normap_type<<endl;
	}	
	if(key == GLFW_KEY_M && action == GLFW_PRESS)
		pd_val+=2.0f;
	if(key == GLFW_KEY_N && action == GLFW_PRESS)
		pd_val-=2.0f;
	if(key == GLFW_KEY_G || key == GLFW_KEY_F ){
		cout<<"DBG_VAL: "<<dbg_val<<endl;
		if(key == GLFW_KEY_G && action == GLFW_PRESS){
			
			dbg_val-=0.5f;
		}
		if(key == GLFW_KEY_F && action == GLFW_PRESS){
			dbg_val+=0.5f;		
		}
	}
	if(key == GLFW_KEY_R || key == GLFW_KEY_T ){
		cout<<"DBG_VAL2: "<<dbg_val2<<endl;
		if(key == GLFW_KEY_R && action == GLFW_PRESS){
			
			dbg_val2-=0.1f;
		}
		if(key == GLFW_KEY_T && action == GLFW_PRESS){
			dbg_val2+=0.1f;		
		}
	}
	
//	cout<<pd_val<<" "<<interp_tiles<<endl;
    // we keep trace of the pressed keys
    // with this method, we can manage 2 keys pressed at the same time:
    // many I/O managers often consider only 1 key pressed at the time (the first pressed, until it is released)
    // using a boolean array, we can then check and manage all the keys pressed at the same time
    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;
}


void genTerrain( GLint* target , int command){
	
	*target = generateTexture();
}

//////////////////////////////////////////
// callback for mouse events
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
      // we move the camera view following the mouse cursor
      // we calculate the offset of the mouse cursor from the position in the last frame
      // when rendering the first frame, we do not have a "previous state" for the mouse, so we set the previous state equal to the initial values (thus, the offset will be = 0)
      if(firstMouse)
      {
          lastX = xpos;
          lastY = ypos;
          firstMouse = false;
      }

      // offset of mouse cursor position
      GLfloat xoffset = xpos - lastX;
      GLfloat yoffset = lastY - ypos;

      // the new position will be the previous one for the next frame
      lastX = xpos;
      lastY = ypos;

      // we pass the offset to the Camera class instance in order to update the rendering
      camera.ProcessMouseMovement(xoffset, yoffset);

}


void boh_func(int* a){
	
	*a=*a+1;
}




void th_genAllTexture(int random_seed,vector<GLfloat> *heightmap, vector<GLfloat> *texmap, bool *work_done_flag){	//funzione da lanciare nel thread
	// nuova heightmap terreno     float freq, float lacunarity , float gain,octaves,int func
	srand(random_seed);				//float freq=0.015, float lacunarity = 2.0, float gain=0.7,int func=0);
	TileGen::getTerrain(rand(),heightmap);
	//generazione heightmap disitrubizione delle texture
	TileGen::getTerrain(rand(),texmap);
	
	*work_done_flag=true;
	
}

void genSingleTexToOpengl( int texture_index,vector<GLfloat> *img){

	GLuint texture;
	int tex_res = N_TILES;
	glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, tex_res, tex_res, 0, GL_RED, GL_FLOAT, img->data());
	
    glGenerateMipmap(GL_TEXTURE_2D);
    // we set how to consider UVs outside [0,1] range
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    // we set the filtering for minification and magnification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	*tail_heightmap = vector<GLfloat>(*img);
	img->clear();
	textureID[texture_index] = texture;
	
}
void genTexToOpenGL(){
	genSingleTexToOpengl(2,generated_texmap);
	genSingleTexToOpengl(5,generated_height);

}

GLint LoadTextureCube(string path)
{
    GLuint textureImage;
    int w, h;
    unsigned char* image;
    string fullname;

    glGenTextures(1, &textureImage);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureImage);

    // we use as convention that the names of the 6 images are "posx, negx, posy, negy, posz, negz", placed at the path passed as parameter
    //POSX
    fullname = path + std::string("purplenebula_rt.tga");
    image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
    if (image == nullptr)
        std::cout << "Failed to load sky POSX!" << std::endl;
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    // we free the memory once we have created an OpenGL texture
    stbi_image_free(image);
    //NEGX
    fullname = path + std::string("purplenebula_lf.tga");
    image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
    if (image == nullptr)
        std::cout << "Failed to load sky NEGX!" << std::endl;
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    // we free the memory once we have created an OpenGL texture
    stbi_image_free(image);
    //POSY
    fullname = path + std::string("purplenebula_up.tga");
    image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
    if (image == nullptr)
        std::cout << "Failed to load sky POSY!" << std::endl;
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    // we free the memory once we have created an OpenGL texture
    stbi_image_free(image);
    //NEGY
    fullname = path + std::string("purplenebula_dn.tga");
    image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
    if (image == nullptr)
        std::cout << "Failed to load sky NEGY!" << std::endl;
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    // we free the memory once we have created an OpenGL texture
    stbi_image_free(image);
    //POSZ
    fullname = path + std::string("purplenebula_bk.tga");
    image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
    if (image == nullptr)
        std::cout << "Failed to load sky POSZ!" << std::endl;
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    // we free the memory once we have created an OpenGL texture
    stbi_image_free(image);
    //NEGZ
    fullname = path + std::string("purplenebula_ft.tga");
    image = stbi_load(fullname.c_str(), &w, &h, 0, STBI_rgb);
    if (image == nullptr)
        std::cout << "Failed to load sky NEGZ!" << std::endl;
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    // we free the memory once we have created an OpenGL texture
    stbi_image_free(image);

    // we set the filtering for minification and magnification
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // we set how to consider the texture coordinates outside [0,1] range
    // in this case we have a cube map, so
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return textureImage;

}

