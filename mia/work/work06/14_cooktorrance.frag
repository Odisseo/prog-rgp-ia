/*
14_cooktorrance.frag: Fragment shader per il modello di illuminazione di Cook-Torrance

NB) utilizzare "13_phong.vert" come vertex shader

autore: Davide Gadia

Programmazione Grafica per il Tempo Reale - a.a. 2016/2017
C.d.L. Magistrale in Informatica
Universita' degli Studi di Milano

*/


#version 330 core

const float PI = 3.14159;

// variabile di output dello shader
out vec4 colorFrag;

// vettore di incidenza della luce (calcolato nel vertes shader)
in vec3 lightDir;
// normale (in coordinate vista)
in vec3 vNormal;
//vettore da vertice a camera (in coordinate vista)
in vec3 vViewPosition;


uniform vec3 diffuseColor; 
uniform float m; // rugosità superficie - 0 : smooth, 1: rough
uniform float F0; // fresnel reflectance at normal incidence
uniform float Kd; // fraction of diffuse reflection (specular reflection = 1 - k)
    


void main()
{
    // normalizzo la normale    
    vec3 N = normalize(vNormal);
    // normalizzo il vettore di incidenza della luce
    vec3 L = normalize(lightDir.xyz);

    // calcolo del coefficiente del modello di Lambert
    float lambertian = max(dot(L,N), 0.0);

    float specular = 0.0;
    
    // se la componente lambertiana è positiva, procedo al calcolo della componente speculare
    if(lambertian > 0.0)
    {
        // il vettore di vista era stato calcolato nel vertex shader, e il valore era stato già negato per avere il verso dal punto alla camera.
        vec3 V = normalize( vViewPosition );

        // calcolo del Half Vector
        vec3 H = normalize(L + V);
       
        // implementazione delle formule viste nelle slide
        // spezzo in componenti

        // preparo gli angoli e i parametri che mi serviranno per il calcolo delle varie componenti
        float NdotH = max(dot(N, H), 0.0); 
        float NdotV = max(dot(N, V), 0.0); 
        float VdotH = max(dot(V, H), 0.0);
        float mSquared = m * m;
        
        // Attenuazione geometrica G
        float NH2 = 2.0 * NdotH;
        float g1 = (NH2 * NdotV) / VdotH;
        float g2 = (NH2 * lambertian) / VdotH;
        float geoAtt = min(1.0, min(g1, g2));

        // Rugosità D
        // Distribuzione di Beckmann
        // posso semplificare la tangente all'esponente cosi':
        // tan = sen/cos -> tan^2 = sen^2/cos^2 -> tan^2 = (1-cos^2)/cos^2
        // l'esponente diventa quindi -(1-cos^2)/(m^2*cos^2) -> (cos^2-1)/(m^2*cos^2)
        float r1 = 1.0 / ( 4.0 * mSquared * pow(NdotH, 4.0));
        float r2 = (NdotH * NdotH - 1.0) / (mSquared * NdotH * NdotH);
        float roughness = r1 * exp(r2);
        
        // Riflettanza di Fresnel F (approx Schlick)
        float fresnel = pow(1.0 - VdotH, 5.0);
        fresnel *= (1.0 - F0);
        fresnel += F0;
        
        // metto tutto assieme per la componente speculare
        specular = (fresnel * geoAtt * roughness) / (NdotV * lambertian * PI);
    }
    
    // calcolo colore finale con anche la componente diffusiva
    vec3 finalColor = diffuseColor * lambertian * (Kd + specular * (1.0 - Kd));
    colorFrag = vec4(finalColor, 1.0);
}