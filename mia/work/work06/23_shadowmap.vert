/*
23_shadowmap.vert: vertex shader per la creazione della shadow map.
Applica ai vertici la matrice di proiezione dal punto di vista della camera, e salva
le coordinate nella depth map

autore: Davide Gadia

Programmazione Grafica per il Tempo Reale - a.a. 2015/2016
C.d.L. Magistrale in Informatica
Universita' degli Studi di Milano

*/


#version 330 core

// posizione vertice in coordinate mondo
layout (location = 0) in vec3 position;

// matrice di proiezione dal punto di vista della luce
uniform mat4 lightSpaceMatrix;

// matrice di modellazione
uniform mat4 modelMatrix;

void main()
{
	  // calcolo posizione vertici in coordinate vista ma rispetto alla luce
    gl_Position = lightSpaceMatrix * modelMatrix * vec4(position, 1.0f);
}
