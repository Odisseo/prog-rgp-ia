/*
24_cooktorrance_tex_dirlight.vert: vertex shader for Cook-Torrance, with texturing.
It consider a single directional light.

author: Davide Gadia

Real-time Graphics Programming - a.a. 2017/2018
Master degree in Computer Science
Universita' degli Studi di Milano

*/
/*
  risoluzione texture 225x225
  numero di quadrati: 225





*/
#version 330 core

// vertex position in world coordinates
layout (location = 0) in vec3 position;
// vertex normal in world coordinate
layout (location = 1) in vec3 normal;
// UV coordinates
layout (location = 2) in vec2 UV;

// model matrix
uniform mat4 modelMatrix;
// view matrix
uniform mat4 viewMatrix;
// Projection matrix
uniform mat4 projectionMatrix;

uniform sampler2D tex;

uniform sampler2D heightmap;

uniform sampler2D previous_heightmap;

uniform float roughness;
uniform float repeat;


uniform int apply_interp;

uniform float pd;
uniform float tile_size;

uniform int vps;  //vertex per side

// normals transformation matrix (= transpose of the inverse of the model-view matrix)
uniform mat3 normalMatrix;

// the light incidence direction of the directional light (passed as uniform)
uniform vec3 lightPos;


// light incidence direction (in view coordinate)
out vec3 lightDir;
out float fog_factor;
// the transformed normal (in view coordinate) is set as an output variable, to be "passed" to the fragment shader
// this means that the normal values in each vertex will be interpolated on each fragment created during rasterization between two vertices

out vec3 vNormal;
out vec3 tangent;
out vec3 bitangent;
out mat3 TBN;
out float height;

// in the fragment shader, we need to calculate also the reflection vector for each fragment
// to do this, we need to calculate in the vertex shader the view direction (in view coordinates) for each vertex, and to have it interpolated for each fragment by the rasterization stage
out vec3 vViewPosition;

// the output variable for UV coordinates
out vec2 interp_UV;
flat out int apply_norm;


vec3 getVertxAtUVDir(vec2 dir){
  vec4 colors = texture(heightmap,UV+dir);
  float h = colors.r;
  vec3 pos = vec3(dir.x,h,dir.y);
  return pos;
}

float getHeightAtUVDir(vec2 dir){
    return (texture(heightmap,UV+dir/vps).r)*roughness;
}

float getPrevHeightAtPos(vec2 pos){
    return (texture(previous_heightmap,pos).r)*roughness;
}

float getWeightedHeight(vec2 pos){
  vec2 UV_pos = UV+(pos)/vps;
  float actual_tile_h = getHeightAtUVDir(pos);
  float prev_tile_h = getPrevHeightAtPos(vec2(UV_pos.x,1.0f-UV_pos.y));
  float prev_tile_influence = (UV_pos.y-0.7f)/0.3f;
  return (actual_tile_h*(1.0f-prev_tile_influence) + prev_tile_h*prev_tile_influence);

}


void main(){


  float h,n,e,s,w;


  if(apply_interp==1){
    h = getWeightedHeight(vec2(0.0f,0.0f));
    if(UV.y>=0.7f){
      
      //if(UV.y>=1.0f-1.0f/vps){
      //  h = getPrevHeightAtPos(vec2(UV.x,1/vps));
      //}
      n = getWeightedHeight(vec2(0.0f,-1.0f));
      e = getWeightedHeight(vec2(1.0f,0.0f));
      w = getWeightedHeight(vec2(-1.0f,0.0f));
      s = getWeightedHeight(vec2(0.0f,1.0f));
/*
  FORMULA
      float old_weight = (UV.y-0.7f)/0.3f;
      float prev_h = texture(previous_heightmap,vec2(UV.x,1.0f-(UV.y))).x;
      h=((h*(1.0f-old_weight))+prev_h*old_weight);  
*/
    }
  }

  if(UV.y<0.7f || apply_interp==0){
    h = getHeightAtUVDir(vec2(0.0f,0.0f));
    if(UV.x>1.0f-1.0f/vps)
      e=h;
    else
      e = getHeightAtUVDir(vec2(1.0f,0.0f));
    if(UV.x<=1.0f/vps)
      w=h;
    else
      w = getHeightAtUVDir(vec2(-1.0f,0.0f));
    if(UV.y>=1.0f-1.0f/vps)
      s=h;
    else
      s = getHeightAtUVDir(vec2(0.0f,1f));

    if(UV.y<= 1.0f/vps){
      n = h;
    }
    else
      n = getHeightAtUVDir(vec2(0.0f,-1f));
  }




  vec3 original_pos = position;
 //float h=0;
  vec4 moved_position = vec4(position,1.0f);
  height = h/roughness;
  //inizialmente lavoro da 0 a 1, per non sballare i calcoli della normale con i valori negativi non trasformo.
  // dato che moltiplico per roughness valori strettamenti positivi, traslo verso il basso in rougness/2 di modo che alla heightmap di altezza 0 corrisponda lo 0 della tile 
  moved_position.y=h-roughness;           
  vec4 mvPosition = viewMatrix * modelMatrix * moved_position;

  apply_norm=0;
  float delta_apply_norm=5000.0f;                       //@ULTIMA 

  //uncomment this to stop bump map at a certain, unprecise, distance
  //if(abs(mvPosition.x)<delta_apply_norm && abs(mvPosition.y)<delta_apply_norm && abs(mvPosition.z)<delta_apply_norm )
    apply_norm=1;


  float x_com,z_com,y_com;
  x_com = w-e;
  z_com = n-s;

/*
    x_com = 10000*(w heightmap  - e heightmap)
*/



  //y_com = 0.5f-x_com-z_com;
/*
  vec3 vlat = normalize(vec3(0.5f,x_com/roughness,0));
  vec3 vlon = normalize(vec3(0.0f,z_com/roughness,-0.5f));
  vec3 norm = cross(vlat,vlon);
  */


 // vec3 norm = normalize( vec3(x_com,tile_size,z_com));     
  vec3 d_x = vec3(-tile_size,x_com,0.0f);
  vec3 d_z = vec3(0.0f,z_com,-tile_size);
  vec3 norm = normalize( cross(d_z,d_x));


  //vec3 norm = vec3((aw+ae)/2,1,(an+as)/2);

  // view direction, negated to have vector from the vertex to the camera
  

  // transformations are applied to the normal
  vNormal = normalize( vec3(normalMatrix  * (norm)));
  tangent = normalize(vec3(normalMatrix *(cross(d_z,norm)  )));//vec3(tile_size,(h-e)/roughness,0))));
  bitangent = normalize(vec3(normalMatrix *(cross(norm,d_x)     )));//vec3(0,(h-n)/roughness,tile_size))));
  TBN = mat3(tangent,bitangent,vNormal);

  vec4 lightPosv = viewMatrix  * vec4(lightPos, 1.0);
  lightDir = normalize(lightPosv.xyz - mvPosition.xyz);

  //fog_factor = (10000 - length(mvPosition)) / (10000-1000);

  // we apply the projection transformation
  gl_Position = projectionMatrix * mvPosition;

  // I assign the values to a variable with "out" qualifier so to use the per-fragment interpolated values in the Fragment shader
  interp_UV = UV;

}
