#ifndef TILEGEN_H
#define TILEGEN_H

#pragma once

#include "utils/FastNoise.cpp"
#include <vector>
#include <glad/glad.h>
#include <cmath>

/*
	
	 
	 * pianura						
	 * pianura con depressioni
	 * pianura con colline basse
	 * a tratti montuoso
	 * catena montuosa
	 * ad altipiani



*/

using namespace std;
class TileGen{

	public:	
	TileGen(){}
	static int n_tiles;
	static float tile_size;
	static int debug;
	static vector<GLfloat> *easy_crater;
	
	static void getTextureMap(int random_seed, vector<GLfloat> *img){
		generic(random_seed,img);
	}
	
	static void getTerrain(int random_seed, vector<GLfloat> *img ){

		float r=rand()%5;
		if(r==0){
			billow(random_seed,img,pv_flatAll);
		}
		else if(r==1){
			generic(random_seed,img,pv_flatAll);
		}
		else if(r==2){billow(random_seed,img,pv_gentleDepressions);}
		else if(r==3){billow(random_seed,img,pv_mountains);}
		else if(r==4){two_hm(random_seed,img,0,pv2_highGround);}
		else if(r==5){two_hm(random_seed,img,1,pv2_dryLand);}
		
		//billow(random_seed,img,pv_flatAll);

		gen_craters(100,img);
			
		
		
	}
	
	/*
	 *			DEVO SEMPRE ritornare un valore tra 0 ed 1
				FastNoise mi da un valore tra -1 e 1, quindi io prendo questi valori e li trasformo in modo che stiano in un intervallo -3 3
				a questo punto, prima di resituirli, divido per 6 e sommo 0.5
	 * 
	 */
	
	
	static void generic(int random_seed, vector<GLfloat> *img, float (*per_vertex_modification)(float) = nullptr){
		float freq = 0.03;
		float lacunarity =2.0f;
		float gain =0.7;
		int n_octaves=8;
		
		GLuint texture;
		FastNoise myNoise; 
		cout<<"RAND "<<random_seed<<endl;
		myNoise.SetSeed(random_seed);
		myNoise.SetNoiseType(FastNoise::ValueFractal);
		myNoise.SetFrequency(freq);
		myNoise.SetFractalLacunarity(lacunarity);
		myNoise.SetFractalGain(gain);
		myNoise.SetFractalOctaves(n_octaves);
		for(int i=0; i<n_tiles; i++){
			for(int j=0; j<n_tiles; j++){
				float result=0.0f;
				if(per_vertex_modification!=nullptr)
					result=(per_vertex_modification(myNoise.GetNoise(j,i)));
				else
					result=((myNoise.GetNoise(j,i)));
				result=(result/6.0f)+0.5f; 			
				img->push_back(result);
			}
		}
	}
	
		static void billow(int random_seed, vector<GLfloat> *img, float (*per_vertex_modification)(float) = nullptr){
		float freq = 0.008;
		float lacunarity =2.0f;
		float gain =0.45;
		int n_octaves=9;
		
		GLuint texture;
		FastNoise myNoise; 
		cout<<"RAND "<<random_seed<<endl;
		myNoise.SetSeed(random_seed);
		myNoise.SetNoiseType(FastNoise::ValueFractal);
		myNoise.SetInterp(FastNoise::Hermite);
		myNoise.SetFrequency(freq);
		myNoise.SetFractalType(FastNoise::Billow);
		myNoise.SetFractalLacunarity(lacunarity);
		myNoise.SetFractalGain(gain);
		myNoise.SetFractalOctaves(n_octaves);
		for(int i=0; i<n_tiles; i++){
			for(int j=0; j<n_tiles; j++){
				float result=0.0f;
				if(per_vertex_modification!=nullptr)				
					result=(( per_vertex_modification(myNoise.GetNoise(j,i))));
				else
					result=(myNoise.GetNoise(j,i));
				result=(result/6.0f)+0.5f; 			// da 0 a 0.5
				img->push_back(result);
					
			}
		}
	}
	
	static void two_hm(int random_seed, vector<GLfloat> *img, int command,float (*per_vertex_modification)(float,float) = nullptr){
		float freq = 0.008;
		float lacunarity =2.0f;
		float gain =0.45;
		int n_octaves=9;
		
		GLuint texture;
		FastNoise myNoise; 		
		FastNoise seconNoise;
		cout<<"RAND "<<random_seed<<endl;
		myNoise.SetSeed(random_seed);
		myNoise.SetNoiseType(FastNoise::ValueFractal);
		myNoise.SetInterp(FastNoise::Hermite);
		myNoise.SetFrequency(freq);
		myNoise.SetFractalType(FastNoise::FBM);
		myNoise.SetFractalLacunarity(lacunarity);
		myNoise.SetFractalGain(gain);
		myNoise.SetFractalOctaves(n_octaves);
		
		seconNoise.SetSeed(rand());
		seconNoise.SetNoiseType(FastNoise::ValueFractal);
		seconNoise.SetInterp(FastNoise::Hermite);

		seconNoise.SetFractalLacunarity(lacunarity);
		seconNoise.SetFractalGain(gain);
		seconNoise.SetFractalOctaves(n_octaves);		
		
		if(command==0){
			seconNoise.SetFractalType(FastNoise::Billow);
			seconNoise.SetFrequency(0.01f);	
		}
		else if(command==1){
			seconNoise.SetFractalType(FastNoise::Billow);
			seconNoise.SetFrequency(0.005f);
			seconNoise.SetFractalGain(0.3f);
		}
		
		for(int i=0; i<n_tiles; i++){
			for(int j=0; j<n_tiles; j++){
				float result=0.0f;
				if(command==0)
					result=(( per_vertex_modification(myNoise.GetNoise(j,i),seconNoise.GetNoise(j,i))));
				else if(command==1)
					result=(( per_vertex_modification(myNoise.GetNoise(j,i),seconNoise.GetNoise(j,i))));
				result=(result/6.0f)+0.5f; 			
				img->push_back(result);
					
			}
		}
	}
	
	
	
	/*
	 * 			pv <- per vertex. da FastNoise ricevo float da -1 a 1 e ritornano da -1 a 1
	 * */
	 
	 
	 
	//depressioni meno ripide ----> colline (billow
	static float pv_gentleDepressions(float val){
		val+=0.3f;
		if(val<0)
			val*=0.1f;
		return val;
	}
	
	//appiana i terreni non in depressione
	static float pv_flatPositive(float val){
		if(val>0)
			val*=0.3;
			
		return val;
	}
	
	//pianura pura
	static float pv_flatAll(float val){
		return val*0.2;
	}
	

	
	static float pv_lonelyMountains(float val){		//così così se applicato con value fractal, meglio billow
		float threshold = 0.10f;
		if(val>threshold){
			val=3.2f*val -0.2f;		//lineare, (0.10,0.10) -> (1.0, 3.0)
		}
		if(val<0)
			val*=0.3f;
		return val;
	}
	
	static float pv_mountains(float val){		//così così se applicato con value fractal, meglio billow
		val+=0.3f;
		if(val>0.0f && val <=0.3f){
			val = 4.0f*val;		 // 0.3 -> 1.2
		}
		else if(val>0.3){		//0.3+ -> 3
			val =  2.57143f *val + 0.428571f;
		}
		return val;
	}
	
	
	/*
	 * 		pv2 <- prende 2 float come argomenti (da 2 hm in corrispondenza)
	 * 	
	 * */
	 
	 // val fractal, cell billow
	 static float pv2_highGround(float val, float cell){
		cell+=0.5f;
		if(cell>0.0f && cell <=0.3f){	// 0 -> 0.3,2
			cell = 6.7f*cell;	
			return cell;
		}
		else if(cell>0.3){		//0.3+ -> 3 (+1.2)
			cell=2.0f+0.5*cell;
			val = val*0.5 + cell;
			return val;
		}
		return cell;
		
	 }
	 
	 // 2hm , command = 1;
	 static float pv2_dryLand(float val, float cell){
		if(cell<0.0f && cell>=-0.5f){
			cell=4.0f*cell+val*0.25f;
		}
		else if(cell<-0.5f){
			cell=cell*0.2f-2.0f+val*0.25f;
		}
		else{
			cell+=val*0.5f;
		}
		return cell;
	 }
	 
	static float gen_craters(int num,vector<GLfloat>*img){
		int x,y;
		float rad;
		for(int i=0; i<num; i++){
			x=rand()%(n_tiles-1);
			y= rand()%(n_tiles-1);
			rad = rand()%50+2.0f;
			//cout<<"Gen crater n"<<i<<" "<<rad<<" x"<<x<<" y"<<y<<endl;
			gen_crater(x,y,rad,img);
		}
		
	}
	
	static float gen_crater(int x,int y,float radius, vector<GLfloat> *img){

		 float i,j,old_i,old_j,last_radius;
		 bool border=true;
		 i=0.0f;
		 j=0.0f;
		 last_radius=radius;
		 old_i=-1.0f;
		 old_j=-1.0f;
		 float max_depth = radius*radius/50000.0f;		
		 while(j<=last_radius){
			 i=sqrt((last_radius*last_radius)-(j*j));
			 if(i!=old_i || j != old_j){
				// cout<<"modifhmStrip"<<x<<" "<<y<<" "<<j<<" "<<i<<" "<<radius<<endl;
				 modifyHmStrip(x,y,(int)j,(int)i,max_depth,img,radius);
			 }
			 old_i=i;
			 old_j=j;
			 j+=0.5f;;
		}
	}
		
	 
	static float getIndex(int x, int y){
		if(x<0||x>n_tiles-1||y<0||y>n_tiles-1)
			return -1;
		return y*n_tiles+x;
	}
	
	static float modifyHmStrip(int center_x, int center_y, int x_offset, int y_offset, float max, vector<GLfloat> *img,float radius){
		int end=center_y-y_offset;
		bool first= true;
		float offset=0.01f;
		for(int i=y_offset;center_y+i>=end;i--){
				float dist = sqrt(pow((x_offset),2) + pow((i),2)); 
				
				offset =radius*radius*pow(dist/(radius),3)/50000.0f - max;  //-(cbrt(1.0f-(dist/((2.0f*radius)))))/10.0f; //- img->at(getIndex(center_x+x_offset,center_y+i));			//
				//cout<<"DBG PD "<<center_x<<" "<<center_y<<" - "<<x_offset<<" "<<i<<" dist:"<<dist<<" rad:"<<radius<<" offs"<<offset<<endl;
				modifyHm(center_x+x_offset,center_y+i,offset,img);
				if(x_offset!=0){
					modifyHm(center_x-x_offset,center_y+i,offset,img);
				}
			
			
			/*
			 * 	offset = (pow(dx/radius,3)/(radius*4.3f))-1.0f/(radius*4.3f);	
			 * 
			 */


		}
		
	}
	
	static void modifyHm(int x, int y, float offset, vector<GLfloat> *img){
		int index = getIndex(x,y);
		if(index!=-1)
			img->at(index)+=offset;
	} 
};
#endif