#ifndef TERRAINTILE_H
#define TERRAINTILE_H

#pragma once
using namespace std;

// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include <glad/glad.h>
#include <math.h>
// GL Includes
#include <glad/glad.h> // Contains all the necessery OpenGL includes
// we use GLM data structures to convert data in the Assimp data structures in a data structures suited for VBO, VAO and EBO buffers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

// Assimp includes
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

// we include the library for image loading
#include <stb_image/stb_image.h>

// we include the Mesh class (v2), which manages the "OpenGL side" (= creation and allocation of VBO, VAO, EBO buffers) of the loading of models
#include <utils/mesh_v2.h>

class TerrainTile{
	
public:
 	int texture_index,alternative_texture,alternative_texture_map, heightmap_index, prev_heightmap_index, prev_alt_map ;
	float quad_size,step_factor;
	int quad_num;
	bool merge_tiles=true;
	Mesh* mesh;

	
	
	TerrainTile(float qs, int s){
		quad_size=qs;
		quad_num=s;
		this->buildMesh();
	};

		void Draw(Shader shader){
		mesh->Draw(shader);
	}

	~TerrainTile(){};		//@TODO
	
	
	float getSize(){
		return quad_size;
	}
	
	int getQuadNum(){
		return quad_num;
	}
	
	void translate(glm::vec3 v){
		this->terrainMatrix = glm::translate(this->terrainMatrix, v);
	}
	
	void uniformTextures(Shader s, vector<GLint> textureID){
		GLint tex_location = glGetUniformLocation(s.Program, "tex");
		GLint height_location = glGetUniformLocation(s.Program, "heightmap");
		GLint prev_height_location = glGetUniformLocation(s.Program, "previous_heightmap");
		GLint map_tex_location = glGetUniformLocation(s.Program,"map_tex");
		GLint alt_tex_location = glGetUniformLocation(s.Program,"alt_tex");
		GLint prev_map_tex_location = glGetUniformLocation(s.Program,"previous_alt_map");
		GLint bump_map_tex_location = glGetUniformLocation(s.Program,"bump_tex");		
		
		glUniform1i(tex_location,texture_index);
		glUniform1i(height_location,heightmap_index);
		glUniform1i(prev_height_location, prev_heightmap_index);
		glUniform1i(map_tex_location,alternative_texture_map);	
		glUniform1i(alt_tex_location,alternative_texture);		
		glUniform1i(prev_map_tex_location,prev_alt_map);
		glUniform1i(bump_map_tex_location,8);		
		
		glActiveTexture(GL_TEXTURE0+texture_index);
		glBindTexture(GL_TEXTURE_2D, textureID[texture_index]);
		glActiveTexture(GL_TEXTURE0+heightmap_index);
		glBindTexture(GL_TEXTURE_2D, textureID[heightmap_index]);		
		glActiveTexture(GL_TEXTURE0+prev_heightmap_index);
		glBindTexture(GL_TEXTURE_2D, textureID[prev_heightmap_index]);
		glActiveTexture(GL_TEXTURE0+alternative_texture);
		glBindTexture(GL_TEXTURE_2D, textureID[alternative_texture]);				
		glActiveTexture(GL_TEXTURE0+alternative_texture_map);
		glBindTexture(GL_TEXTURE_2D, textureID[alternative_texture_map]);		
		glActiveTexture(GL_TEXTURE0+prev_alt_map);
		glBindTexture(GL_TEXTURE_2D, textureID[prev_alt_map]);		
		glActiveTexture(GL_TEXTURE0+8);
		glBindTexture(GL_TEXTURE_2D, textureID[8]);	
				
	}
	
	void setTexIndex(int basic_tex, int alt_tex, int map, int prev_map){
		texture_index=basic_tex;
		alternative_texture=alt_tex;
		alternative_texture_map=map;
		prev_alt_map = prev_map;
	}
	void setHeightmapIndex(int tile,int prev_tile){
		heightmap_index=tile;
		prev_heightmap_index=prev_tile;
	}
	
	void prepareDefaultUniforms(Shader s,glm::mat4 view){

		GLint roughness_loc = glGetUniformLocation(s.Program,"roughness");
		GLint modelMatrix_loc = glGetUniformLocation(s.Program,"modelMatrix");
		GLint normalMatrix_loc = glGetUniformLocation(s.Program,"normalMatrix");
		GLint repeat_loc = glGetUniformLocation(s.Program,"repeat");
		GLint apply_interp_loc = glGetUniformLocation(s.Program,"apply_interp");
		GLint vertex_per_side_location = glGetUniformLocation(s.Program,"vps");
		
		glUniform1f(roughness_loc,this->tile_roughness);
		glUniformMatrix4fv(modelMatrix_loc, 1, GL_FALSE, glm::value_ptr(this->terrainMatrix));
		this->normalMatrix = glm::inverseTranspose(glm::mat3(view*this->terrainMatrix));
		glUniformMatrix3fv(normalMatrix_loc, 1, GL_FALSE, glm::value_ptr(this->normalMatrix));
		glUniform1f(repeat_loc, quad_num);
		glUniform1i(apply_interp_loc, (int) merge_tiles );
		glUniform1i(vertex_per_side_location, quad_num+1);

	}
	
	void getPosition(glm::vec3 *out){
		out->x = terrainMatrix[3][0];
		out->y = terrainMatrix[3][1];
		out->z = terrainMatrix[3][2];
	}
	
	
	
private: 	
/*
 * 					tl(index+1)    tr(index+3)
 * 	                   \    2     / 
 * 			             \      /
 *					1   	c(index+0)4									
 * 		                 /      \
 *                     /    3     \
 * 					br(index+2)   bl(index+4)
 * */
 
	float tile_roughness=2000.0f;

	glm::mat4 terrainMatrix;
	glm::mat3 normalMatrix;
	
	float mean(float x, float y){
		return (x+y)/2.0f;
	}
	float mean(float a, float b,float c, float d){
	return ((a+b+c+d)/4.0f);
	}

	void buildMesh(){
		Vertex *toAdd = new Vertex(); 
		mesh = new Mesh();
		mesh->vertices = vector<Vertex>();
		mesh->indices = vector<GLuint>();
		mesh->textures  = vector<Texture>();
		vector<Vertex> *vertices = &mesh->vertices;
		vector<GLuint> *indices = &mesh->indices;
		
		GLuint vertex_count = 0;
		for(int i=0; i< quad_num+1; i++){				
			for(int j=0; j<quad_num+1;j++){
				toAdd->Position = glm::vec3(j*quad_size,0,i*quad_size);
				toAdd->Normal = glm::vec3(0,1,0);
				toAdd->Tangent = glm::vec3(1,0,0);
				toAdd->Bitangent= glm::vec3(0,0,1);
				toAdd->TexCoords = glm::vec2(((float)j)/((float)quad_num),((float)i)/((float)quad_num));
				vertices->push_back(*toAdd);
				vertex_count++;
			}
		} 
		delete toAdd;
		GLuint cvi=vertex_count;
		GLuint vi=0;  //vertex_index
		
		GLfloat h,e,w,s,n,ne,nw,se,sw;
		GLfloat offset = quad_size/2.0f;
		
		
		float tex_max_coord = quad_size*(quad_num+1);
		for(int i=0; i< (quad_num); i+=1){			
			for(int j=0; j<(quad_num);j+=1){
				int vx = j;
				int vy = i;
				
/*				
				h= heights.at(i).at(j);                          // get height in all directions
				s = (i+1<quad_num) ? heights.at(i+1).at(j) : h;		
				n = (i-1>=0 ) ? heights.at(i-1).at(j) : h;
				e = (j<quad_num-1 )? heights.at(i).at(j+1) : h;
				w = (j-1>=0)? heights.at(i).at(j-1):h;
				ne = (i-1>=0 && j+1<=quad_num)? heights.at(i-1).at(j+1) : h;
				se = (i+1<=quad_num && j+1< quad_num) ? heights.at(i+1).at(j+1):h;
				sw = (i+1<=quad_num && j-1>=0 )? heights.at(i+1).at(j-1):h;
				nw =(j-1>=0 && i-1>=0) ? heights.at(i-1).at(j-1) : h;
				//cout<<"H"<<h<<" "<<sw<<endl;

				Vertex main_vertex;
				main_vertex.Position = glm::vec3(vx*quad_size+offset,0,vy*quad_size+offset);
				main_vertex.Normal = glm::vec3(0,1,0);
				main_vertex.Tangent = glm::vec3(1,0,0);
				main_vertex.Bitangent = glm::vec3(0,0,1);
				main_vertex.TexCoords = glm::vec2((i+0.5f)/quad_num,(j+0.5f)/quad_num);
				
			*/	
				
				//cout<<"top_left"<<vi<<" top_right"<<vi+1<<" bottom_left"<<vi+quad_num+1<<" bottom_right"<<vi+quad_num+2 <<endl;
				
				//vertices.at(vi).Position.y=mean(h,nw,w,n);			  //top left
/*				
				vertices.at(vi).TexCoords= glm::vec2(((float)j)/quad_num,((float)i)/quad_num);
				
				//vertices.at(vi+1).Position.y=mean(h,ne,e,n);          //top right
				if(j==quad_num-1)
					vertices.at(vi+1).TexCoords= glm::vec2((j+1.0f)/quad_num,(i)/quad_num);
					
				
				if(i==quad_num-1){
				//vertices.at(vi+quad_num+1).Position.y= mean(sw,s,w,h); //bottom left
					vertices.at(vi+quad_num+1).TexCoords= glm::vec2(((float)j)/quad_num,(i+1.0f)/quad_num);      
					
					//vertices.at(vi+quad_num+2).Position.y=mean(h,se,e,s);          //bottom right
					if(j==quad_num-1)
						vertices.at(vi+quad_num+2).TexCoords= glm::vec2((j+1.0f)/quad_num,(i+1.0f)/quad_num);
				
				}
				 * */
				//glm::vec3 fnw,fn,fne,fe,fse,fs,fsw,fs,fl,fu,fr,fd;
				
				//vertices.push_back(main_vertex);
				
				indices->push_back(vi);					//left face
				indices->push_back(vi+quad_num+1);
				indices->push_back(vi+quad_num+2);
				
				indices->push_back(vi);					//right face
				indices->push_back(vi+1);
				indices->push_back(vi+quad_num+2);

				
				vi+=1;
				cvi+=1;
				
				
				
			}
			vi++;
		}
		mesh->setupMesh();
	};
		

};

#endif // TERRAINTILE_H
