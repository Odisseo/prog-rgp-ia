#ifndef BOHNOISE_H
#define BOHNOISE_H


#pragma once
using namespace std;

#include <vector>
#include <stb_image/stb_image.h>
#include <math.h>

class BohNoise{
	public:	
	vector<vector<float>> data;
	int size;
	
	BohNoise(int s){
		size=s;
	}	
	
	void getDataAsGLfloatArray( GLfloat *toRet){
		int cnt=0;
		for(int i=0; i<size; i++){
			for(int j=0;j<size;j++){
				toRet[cnt]=(GLfloat) data.at(i).at(j);
				cnt++;
			}
		}
		
	}
	
	void resultFromImage(const char* path){
		int w, h, channels;
		unsigned char* image;
		int result[size][size];
		image = stbi_load(path, &w, &h, &channels, STBI_rgb);
		int img_size = w;
		int sampling_step = (img_size/size);
		cout<<"Sampling_step = "<<sampling_step<<" img_size:"<<w<<" "<<h<<" "<<channels<<endl;
		int row_count=0;
		int col_count=0;
		
		for(int i=0; i<size;i++){
			vector<float> val;
			for(int j=0; j<size;j++){
				unsigned char* pixelOffset = image + (i*sampling_step + h * j*sampling_step) * channels;
				unsigned char r = pixelOffset[0];
				unsigned char g = pixelOffset[1];
				unsigned char b = pixelOffset[2];
				unsigned char a = channels >= 4 ? pixelOffset[3] : 0xff;				
				val.push_back((float)r/255.0f);
				//cout<<r/255.0f<<" ";
			}
			//cout<<endl;
			data.push_back(val);
		}
			//cout<<" data size"<<data.size()<<endl;
		
	}
	
	private:
	
	
	
	
};

#endif;